import os

os.system("apt-get update && \
   apt-get install -y python-dev && \
   curl -O https://bootstrap.pypa.io/get-pip.py && \
   python get-pip.py && \
   pip install awscli && \
   aws deploy push --application-name Calculator --s3-location s3://pied-pipers-atlassian-dev/calculator-pipeline-$BITBUCKET_BUILD_NUMBER --ignore-hidden-files")
